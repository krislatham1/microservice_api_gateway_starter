package com.rubygreen.exception;

public class ErrorInfo {

    private String message;
    private String details;

    public ErrorInfo(String message, String details) {
        this.message = message;
        this.details = details;
    }

    @Override
    public String toString() {
        return "ErrorInfo{" +
                "message='" + message + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
