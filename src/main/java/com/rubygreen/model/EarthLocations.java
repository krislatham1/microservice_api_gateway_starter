package com.rubygreen.model;

import lombok.Data;

@Data
public class EarthLocations {

    private Object _id;
    private Long id;
    private String privateEntry;
    private String coordinates;
    private String link;
    private String description;
    private String age;
    private String category;
    private String country;
    private String continent;
    private Integer rating;

}
