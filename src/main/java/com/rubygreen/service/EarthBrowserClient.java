package com.rubygreen.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "${earth.location.feign.client.url}", name = "earthLocationClient")
public interface EarthBrowserClient {

    @GetMapping("/locations")
    String getLocations();

    @GetMapping("/parseSingleKmlFile")
    String importSingleKmlFile(@RequestParam String fileName);
}
