package com.rubygreen.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.rubygreen.model.EarthLocations;
import com.rubygreen.service.EarthBrowserClient;
import com.rubygreen.utility.JsonFeignResponseMapper;
import org.modelmapper.ModelMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class ApiGatewayController {
	private static final Logger log = LoggerFactory.getLogger(ApiGatewayController.class);

	private EarthBrowserClient earthlocationClient;
	private JsonFeignResponseMapper jsonResponseMapper;

	@Autowired
	public ApiGatewayController(
			EarthBrowserClient earthBrowserClient,
			JsonFeignResponseMapper jsonResponseMapper) {

		this.earthlocationClient = earthBrowserClient;
		this.jsonResponseMapper = jsonResponseMapper;
	}

	@GetMapping("/locations")
	public List<?> getAllLocations() throws JsonProcessingException {
		return jsonResponseMapper.mapJsonList(
			earthlocationClient.getLocations()
		);
	}

	@GetMapping("/locations_test")
	public List<EarthLocations> getAllLocationsTest() throws JsonProcessingException {
		// Rough example of converting back to a standard POJO
		// Convert to a object list
		List l = new ArrayList(Arrays.asList(
			jsonResponseMapper.doDeserialization(
				earthlocationClient.getLocations()
			)
		));

		// Create Type
		Type listType = new TypeToken<List<EarthLocations>>() {}.getType();

		// Map to DTO
		ModelMapper mapper = new ModelMapper();
		List<EarthLocations> locations = mapper.map(l, listType);

		return locations;
	}

	@GetMapping("/parseSingleKmlFile")
	public List<?> GetSingleKmlFile(@RequestParam String fileName) throws JsonProcessingException {
		return jsonResponseMapper.mapJsonList(
			earthlocationClient.importSingleKmlFile(fileName)
		);
	}

}
