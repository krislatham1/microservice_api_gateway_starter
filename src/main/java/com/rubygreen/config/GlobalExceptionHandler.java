package com.rubygreen.config;

import com.rubygreen.exception.ErrorInfo;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.xml.bind.JAXBException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(JAXBException.class)
    public ResponseEntity<Object> handleJaxbException(JAXBException ex, WebRequest request) {
        ErrorInfo bodyOfResponse = new ErrorInfo("General error.", ex.toString());

        return handleExceptionInternal(ex, bodyOfResponse.toString(),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleCoreException(Exception ex, WebRequest request) {
        ErrorInfo bodyOfResponse = new ErrorInfo("General error.", ex.toString());

        return handleExceptionInternal(ex, bodyOfResponse.toString(),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
